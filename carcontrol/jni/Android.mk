LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE    := libspotify
LOCAL_SRC_FILES := libspotify.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_SHARED_LIBRARIES := libspotify
LOCAL_MODULE    := spotifywrapper
LOCAL_LDLIBS := -L$(SYSROOT)/usr/lib -llog -lOpenSLES
LOCAL_SRC_FILES := jni_interface.cpp logger.cpp runner.cpp task_execute.cpp sound_driver.cpp
LOCAL_CPPFLAGS = -std=c++0x -D__STDC_INT64__
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)
include $(BUILD_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := uvccamera
LOCAL_LDLIBS := -llog -ljnigraphics
LOCAL_SRC_FILES := camera/webcam.c camera/yuv.c camera/video_device.c camera/util.c camera/capture.c
LOCAL_CFLAGS := -std=c99
include $(BUILD_SHARED_LIBRARY)