#ifndef __WEBCAM_H__
#define __WEBCAM_H__

#include <jni.h>
#include "util.h"

static int DEVICE_DESCRIPTOR = -1;
int* RGB_BUFFER = NULL;
int* Y_BUFFER = NULL;

jint Java_com_broadspeed_carcontrol_camera_UVCCamera_startCamera(JNIEnv* env, jobject thiz, jstring deviceName, jint width, jint height);
void Java_com_broadspeed_carcontrol_camera_UVCCamera_loadNextFrame(JNIEnv* env, jobject thiz, jobject bitmap);
jboolean Java_com_broadspeed_carcontrol_camera_UVCCamera_cameraAttached(JNIEnv* env, jobject thiz);
void Java_com_broadspeed_carcontrol_camera_UVCCamera_stopCamera(JNIEnv* env, jobject thiz);

#endif
