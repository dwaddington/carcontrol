#ifndef __CAPTURE_H__
#define __CAPTURE_H__

#include <jni.h>
#include <stdio.h>

#include "video_device.h"

int start_capture(int fd);
int read_frame(int fd, buffer* frame_buffers, int width, int height, int* rgb_buffer, int* y_buffer);
int stop_capturing(int fd);
void process_camera(int fd, buffer* frame_buffers, int width, int height, int* rgb_buffer, int* ybuf);
void stop_camera();

#endif
