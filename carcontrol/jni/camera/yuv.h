#ifndef __YUV_H__
#define __YUV_H__

#include <jni.h>
#include <stdio.h>

int YUV_LOOKUP_TABLE[5][256];

void cache_yuv_lookup_table(int table[5][256]);

void yuyv422_to_argb(unsigned char *src, int width, int height, int* rgb_buffer, int* y_buffer);

#endif

