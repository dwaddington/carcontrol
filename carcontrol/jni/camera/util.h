#ifndef __UTIL_H__
#define __UTIL_H__

#include <jni.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <android/log.h>

#define LOG_TAG "BroadspeedUVCCamDriver"
#define LOGI(...) __android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__)
#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__)

#define CLEAR(x) memset(&(x), 0, sizeof(x))

#define ERROR_LOCAL -1
#define SUCCESS_LOCAL 0

int errnoexit(const char *s);
int xioctl(int fd, int request, void *arg);

#endif
