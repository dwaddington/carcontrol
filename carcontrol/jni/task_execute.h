#ifndef __TASK_EXECUTE_H__
#define __TASK_EXECUTE_H__

#include <list>
#include <string>

#include "libspotify/api.h"

using namespace std;

void* run_simple_callback();

void login(list<int> int_params, list<string> string_params, sp_session *session, sp_track *track);
void load_playlist(list<int> int_params, list<string> string_params, sp_session *session, sp_track *track);
void playlist_container_loaded(sp_playlistcontainer *pc, void *userdata);
void loaded_playlist(sp_playlist *pl, void *userdata);
void populate_active_track(list<int> int_params, list<string> string_params, sp_session *session, sp_track *track);
void image_loaded(sp_image *image, void *userdata);
void toggle_play(list<int> int_params, list<string> string_params, sp_session *session, sp_track *track);
void load_and_play_track(sp_session *session, sp_track *track);
void pause(list<int> int_params, list<string> string_params, sp_session *session, sp_track *track);


void on_logged_in(list<int> int_params, list<string> string_params, sp_session *session, sp_track *track);

#endif
