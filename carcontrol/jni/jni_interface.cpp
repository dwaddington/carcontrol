#include <stdlib.h>
#include <pthread.h>
#include <string.h>
#include "libspotify/api.h"

#include "jni_interface.h"
#include "logger.h"
#include "runner.h"
#include "task_execute.h"
#include "sound_driver.h"

JavaVM* gJvm = nullptr;
static jobject gClassLoader;
static jmethodID gFindClassMethod;

JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM *jvm, void *reserved) {
    gJvm = jvm;
    auto env = getEnv();

    auto ourClass = env->FindClass("com/broadspeed/carcontrol/SpotifyWrapper");
    auto classClass = env->FindClass("java/lang/Class");
    auto classLoaderClass = env->FindClass("java/lang/ClassLoader");
    auto getClassLoaderMethod = env->GetMethodID(classClass, "getClassLoader", "()Ljava/lang/ClassLoader;");

    jobject tmp_cls_loader = env->CallObjectMethod(ourClass, getClassLoaderMethod);
    gClassLoader = env->NewGlobalRef(tmp_cls_loader);

    gFindClassMethod = env->GetMethodID(classLoaderClass, "findClass", "(Ljava/lang/String;)Ljava/lang/Class;");
    return JNI_VERSION_1_6;
}

JNIEXPORT void JNICALL Java_com_broadspeed_carcontrol_SpotifyWrapper_init(JNIEnv *env, jobject thiz, jstring j_storage_path) {
    static pthread_t tid;
    static const char *storage_path;

    storage_path = env->GetStringUTFChars(j_storage_path, 0);
    pthread_setname_np(tid, "Spotify Thread");
    pthread_create(&tid, NULL, start_spotify, (void *) storage_path);

    init_audio_player();
}

jclass findClass(JNIEnv *inEnv, const char* name) {
    return static_cast<jclass>(inEnv->CallObjectMethod(gClassLoader, gFindClassMethod, inEnv->NewStringUTF(name)));
}

JNIEXPORT void JNICALL Java_com_broadspeed_carcontrol_SpotifyWrapper_login(JNIEnv *env, jclass jc, jstring j_username, jstring j_password) {
    const char *username = env->GetStringUTFChars(j_username, 0);
    const char *password = env->GetStringUTFChars(j_password, 0);

    list<string> string_params;
    string_params.push_back(username);
    string_params.push_back(password);
    addTask(login, "login", string_params);

    env->ReleaseStringUTFChars(j_username, username);
    env->ReleaseStringUTFChars(j_password, password);
}

JNIEXPORT void JNICALL Java_com_broadspeed_carcontrol_SpotifyWrapper_searchPlaylist(JNIEnv *env, jclass jc, jint j_playlistIndex) {

    list<int> int_params;
    int_params.push_back((int) j_playlistIndex);
    addTask(load_playlist, "loadPlaylist", int_params);

}

JNIEnv* getEnv() {
    JNIEnv *env;
    int status = gJvm->GetEnv((void **)&env, JNI_VERSION_1_6);
    if(status < 0) {
        log("attaching to current thread");
        status = gJvm->AttachCurrentThread(&env, NULL);
        if(status < 0) {
            return nullptr;
        }
    }
    log("returning env variable");
    return env;
}

JNIEXPORT void JNICALL Java_com_broadspeed_carcontrol_SpotifyWrapper_togglePlay(JNIEnv *env, jclass jc, jint track_id){
    int t_id = (int) track_id;
    list<int> int_params;
    int_params.push_back(t_id);
    addTask(toggle_play,"play_song",int_params);
    log("Track id %d selected", t_id);
}

