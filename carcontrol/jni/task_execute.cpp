#include "task_execute.h"
#include "jni_interface.h"
#include "logger.h"
#include "runner.h"

#include <unistd.h>

static sp_playlistcontainer_callbacks pc_callbacks = {
    NULL,
    NULL,
    NULL,
    &playlist_container_loaded,
};

static sp_playlist_callbacks pl_callbacks = {
    NULL,   //tracks added
    NULL,   //tracks removes
    NULL,   //tracks moved
    NULL,   //renamed
    &loaded_playlist,   //state_changed
    NULL,   //update in progress
    NULL,   //metadata updated
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
};

static bool s_is_playing = false;
static bool s_is_waiting_for_metadata = false;
static bool s_play_after_loaded = false;
static int cur_track_index = -1;
static int s_player_position = 0;


void* run_simple_callback() {
    //Get the JNI Environment variable and the current JVM we're executing code
    //under
    JNIEnv *env = getEnv();
    JavaVM* jvm;
    int gotVM = env->GetJavaVM(&jvm);

    //Now get the class that we want to execute shit under
    jclass ourClass = findClass(env, "com/broadspeed/carcontrol/SpotifyWrapper");

    jmethodID methodId = env->GetStaticMethodID(ourClass, "loggedIn_cback", "(Ljava/lang/String;)V");
    env->CallStaticVoidMethod(ourClass, methodId, env->NewStringUTF("calling calling calling"));

    //Have to Have to Have to call this motherfucker other wise...
    //BOOOOOOOOOOOOOOOMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
    //red everywhere
    jvm->DetachCurrentThread();
}

void loaded_playlist(sp_playlist *pl, void *userdata) {
    if(playlist_vector_index < 50) {
        playlist_vectors[playlist_vector_index] = pl;
        playlist_vector_index++;

        JNIEnv *env = getEnv();
        JavaVM* jvm;
        int gotVM = env->GetJavaVM(&jvm);
        jclass ourClass = findClass(env, "com/broadspeed/carcontrol/SpotifyWrapper");
        jmethodID methodId = env->GetStaticMethodID(ourClass, "addPlaylist", "(ILjava/lang/String;Ljava/lang/String;I)V");

        const char *playlist_name   = sp_playlist_name(pl);
        const char *playlist_desc   = sp_playlist_get_description(pl);
        int playlist_track_count    = sp_playlist_num_tracks(pl);
        if(sp_playlist_is_loaded(pl)){
            jstring fuck1;
            jstring fuck2;
            fuck1 = env->NewStringUTF(playlist_name);
            fuck2 = env->NewStringUTF(playlist_desc);
            env->CallStaticVoidMethod(ourClass, methodId, (playlist_vector_index-1), fuck1, fuck2, playlist_track_count);
            env->DeleteLocalRef(fuck1);
            env->DeleteLocalRef(fuck2);
        } else {
            log("playlist not loaded");
        }

        jvm->DetachCurrentThread();
    }
}


void load_playlist(list<int> int_params, list<string> string_params, sp_session *session, sp_track *track) {
    log("load playlist method called");
    if(session == NULL) {
        exitl("logged in before session initialised");
    }
    int playlist_id = int_params.front();
    int i;
    active_pl = playlist_vectors[playlist_id];

    JNIEnv *env = getEnv();
    JavaVM* jvm;
    int gotVM = env->GetJavaVM(&jvm);
    jclass ourClass = findClass(env, "com/broadspeed/carcontrol/SpotifyWrapper");
    jmethodID methodId = env->GetStaticMethodID(ourClass, "addTrackToCurPlaylist", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V");


    for(i = 0; sp_playlist_num_tracks(active_pl); ++i) {
        sp_track *cur_track = sp_playlist_track(active_pl, i);
        if(cur_track == NULL) {
            break;
        }
        int num_artists = sp_track_num_artists(cur_track);
        const char *track_name = sp_track_name(cur_track);
        sp_album *track_album = sp_track_album(cur_track);
        const char *track_album_str = sp_album_name(track_album);

        string artists = "";
        int track_duration = sp_track_duration(cur_track);

        int ca;
        for(ca = 0; ca < num_artists; ++ca) {
            sp_artist *cur_artist = sp_track_artist(cur_track, ca);
            if(strcmp(artists.c_str(), "") == 0) {
                artists = artists + sp_artist_name(cur_artist);
            } else {
                artists = artists + ", " + sp_artist_name(cur_artist);
            }
        }
        jstring fuck1;
        jstring fuck2;
        jstring fuck3;
        fuck1 = env->NewStringUTF(track_name);
        fuck2 = env->NewStringUTF(artists.c_str());
        fuck3 = env->NewStringUTF(track_album_str);
        env->CallStaticVoidMethod(ourClass, methodId, fuck1, fuck2, fuck3, track_duration);
        env->DeleteLocalRef(fuck1);
        env->DeleteLocalRef(fuck2);
        env->DeleteLocalRef(fuck3);

    }
    jvm->DetachCurrentThread();
}

void playlist_container_loaded(sp_playlistcontainer *pc, void *userdata) {
    int num_playlists = sp_playlistcontainer_num_playlists(pc);
    int i;

    JNIEnv *env = getEnv();
    JavaVM* jvm;
    int gotVM = env->GetJavaVM(&jvm);
    jclass ourClass = findClass(env, "com/broadspeed/carcontrol/SpotifyWrapper");

    log("playlists loaded: %d", num_playlists);
    for(i = 0; i < sp_playlistcontainer_num_playlists(pc); ++i) {
        sp_playlist *pl = sp_playlistcontainer_playlist(pc, i);
        sp_playlist_add_callbacks(pl, &pl_callbacks, NULL);
    }
    jmethodID methodId = env->GetStaticMethodID(ourClass, "setPlaylistCount", "(I)V");
    env->CallStaticVoidMethod(ourClass, methodId, num_playlists);

    env->DeleteLocalRef(ourClass);
    jvm->DetachCurrentThread();
}

void login(list<int> int_params, list<string> string_params, sp_session *session, sp_track *track) {
    log("login method called");

    if(session == NULL) {
        exitl("logged in before session initialised");
    }
    string username = string_params.front();
    string password = string_params.back();
    sp_session_login(session, username.c_str(), password.c_str(), true, NULL);
    log("login method finished calling");
}

void on_logged_in(list<int> int_params, list<string> string_params, sp_session *session, sp_track *track) {
    sp_error error = (sp_error)int_params.front();
    bool success = (SP_ERROR_OK == error) ? true : false;

    JNIEnv *env = getEnv();
    JavaVM* jvm;
    int gotVM = env->GetJavaVM(&jvm);

    jclass ourClass = findClass(env, "com/broadspeed/carcontrol/SpotifyWrapper");
    jmethodID methodId = env->GetStaticMethodID(ourClass, "loggedIn_cback", "(Ljava/lang/String;)V");
    env->CallStaticVoidMethod(ourClass, methodId, env->NewStringUTF(sp_error_message(error)));
    env->DeleteLocalRef(ourClass);

    //Call other setup methods to give us some info for our UI
    sp_playlistcontainer *test_playlist = sp_session_playlistcontainer(session);
    playlists = test_playlist;
    if(NULL == test_playlist){
        log("not logged in apparently");
    }
    sp_playlistcontainer_add_callbacks(playlists, &pc_callbacks, NULL);

    jvm->DetachCurrentThread();
}

void populate_active_track(list<int> int_params, list<string> string_params, sp_session *session, sp_track *track){
    log("hit populate_active_track()");
    if(track != NULL) {
        if(s_is_playing) {
            log("s_is_playing");
            sp_session_player_play(session, false);
        }
        log("unloading");
        sp_session_player_unload(session);
        sp_track_release(track);
    }

    int t_id = int_params.front();
    cur_track_index = t_id;
    track = sp_playlist_track(active_pl, t_id);
    sp_track_add_ref(track);
    s_player_position = 0;

    if(sp_track_is_loaded(track)) {
        log("sp_track_is_loaded");
        load_and_play_track(session, track);
    } else {
        log("waiting for metadata");
        s_is_waiting_for_metadata = true;
    }
}

void play_track(sp_session *session, sp_track *track) {
    log("called play_track");
    sp_session_player_play(session, true);
    s_is_playing = true;
}

void load_and_play_track(sp_session *session, sp_track *track) {
    sp_album *track_album = sp_track_album(track);
    const byte *cover_id = sp_album_cover(track_album, SP_IMAGE_SIZE_NORMAL);
    sp_image *sptfy_image = sp_image_create(session, cover_id);
    sp_image_add_load_callback(sptfy_image, &image_loaded, NULL);

    sp_session_player_load(session, track);
    log("sp_session_player_load complete");
    if(s_play_after_loaded) {
        log("s_play_after_loaded == false");
        log("calling play_track");
        play_track(session, track);
    }
}

void load_and_play_track_after_metadata_updated(list<int> int_params, list<string> string_params, sp_session *session, sp_track *track) {
    if(s_is_waiting_for_metadata == true && sp_track_is_loaded(track)) {
        s_is_waiting_for_metadata = false;
        load_and_play_track(session, track);
    }
}


void play_next(list<int> int_params, list<string> string_params, sp_session *session, sp_track *track) {
    int t_id = int_params.front();
    s_play_after_loaded = s_is_playing;
    populate_active_track(int_params, string_params, session, track);
}

void toggle_play(list<int> int_params, list<string> string_params, sp_session *session, sp_track *track) {
    int t_id = int_params.front();

    if(!s_is_playing) {
        log("s_isnt_playing");
        if(cur_track_index == t_id) {
            log("cur_track_index == t_id");
            play_track(session, track);
        }
        else {
            log("called populate active track");
            s_play_after_loaded = true;
            populate_active_track(int_params, string_params, session, track);
        }
    } else {
        log("s_is_playing");
        if(cur_track_index == t_id) {
            pause(int_params, string_params, session, track);
        } else {
            log("called populate active track");
            s_is_playing = false;
            s_play_after_loaded = true;
            populate_active_track(int_params, string_params, session, track);
        }
    }
}

void pause(list<int> int_params, list<string> string_params, sp_session *session, sp_track *track) {
    if(s_is_playing) {
        s_is_playing = false;
        sp_session_player_play(session, false);
    }
}

void image_loaded(sp_image *image, void *userdata) {
    if(sp_image_is_loaded(image)) {
        log("Successfully loaded the image");
        size_t size;
        const void *data = sp_image_data(image, &size);
        unsigned char (*test)[size] = (unsigned char (*)[size]) data;

        JNIEnv *env = getEnv();
        JavaVM* jvm;
        int gotVM = env->GetJavaVM(&jvm);

        jbyteArray Array = env->NewByteArray(size);
        env->SetByteArrayRegion(Array, 0, size, (jbyte*)test);

        jclass ourClass = findClass(env, "com/broadspeed/carcontrol/SpotifyWrapper");
        jmethodID methodId = env->GetStaticMethodID(ourClass, "loadTrackImage", "([B)V");
        env->CallStaticVoidMethod(ourClass, methodId, Array);

        jvm->DetachCurrentThread();
    } else {
        log("Failed to load the image");
    }
}
