#ifndef __RUN_LOOP_H__
#define __RUN_LOOP_H__

#include <list>
#include <vector>
#include <string>
#include "libspotify/api.h"

using namespace std;

static sp_playlistcontainer *playlists = NULL;
static sp_playlist *playlist_vectors[50];
static int playlist_vector_index = 0;
static sp_playlist *active_pl = NULL;

typedef void (*task_fptr)(list<int> int_params, list<string> string_params, sp_session *session, sp_track *track);

void addTask(task_fptr fptr, string name, list<float> int_params, list<string> string_params);
void addTask(task_fptr fptr, string name, list<string> string_params);
void addTask(task_fptr fptr, string name, list<int> int_params);
void addTask(task_fptr fptr, string name);

void* start_spotify(void *storage_location);

#endif
