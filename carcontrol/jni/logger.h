#ifndef __LOGGER_H__
#define __LOGGER_H__

void log(const char *fmt, ...);
void exitl(const char *fmt, ...);
void logPlayback(const char *fmt, ...);
void logPlayTrack(const char *fmt, ...);

#endif
