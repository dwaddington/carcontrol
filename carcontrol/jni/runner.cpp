#include <pthread.h>
#include <errno.h>
#include <list>
#include <string.h>

#include "runner.h"
#include "task_execute.h"
#include "logger.h"
#include "key.h"

using namespace std;

int music_delivery(sp_session *sesh, const sp_audioformat *format, const void *frames, int num_frames);

static sp_track *s_track = NULL;

struct Task {
    task_fptr fptr;
    list<int> int_params;
    list<string> string_params;
    string name;

    Task(task_fptr _fptr, string _name, list<int> _int_params, list<string> _string_params) :
            fptr(_fptr), name(_name), int_params(_int_params), string_params(_string_params) {
    }
};

static pthread_mutex_t s_notify_mutex;
static pthread_cond_t s_notify_cond;

static list<Task> s_tasks;

static int s_next_timeout = 0;

void addTask(task_fptr fptr, string name, list<int> int_params, list<string> string_params) {
    log("Add task <%s> to the queue", name.c_str());
    pthread_mutex_lock(&s_notify_mutex);
    Task task(fptr, name, int_params, string_params);
    s_tasks.push_back(task);
    pthread_cond_signal(&s_notify_cond);
    pthread_mutex_unlock(&s_notify_mutex);
}

void addTask(task_fptr fptr, string name, list<string> string_params) {
    list<int> int_params;
    addTask(fptr, name, int_params, string_params);
}

void addTask(task_fptr fptr, string name, list<int> int_params) {
    list<string> string_params;
    addTask(fptr, name, int_params, string_params);
}

void addTask(task_fptr fptr, string name) {
    list<int> int_params;
    list<string> string_params;
    addTask(fptr, name, int_params, string_params);
}

static void logged_in(sp_session *session, sp_error error) {
    log("spotify logged_in callback called");
    list<int> int_params;
    int_params.push_back(error);

    addTask(on_logged_in, "login_callback", int_params);
}

static void process_events(list<int> int_params, list<string> string_params, sp_session *session, sp_track *track) {
    do {
        sp_session_process_events(session, &s_next_timeout);
    } while (s_next_timeout == 0);
}

static void notify_main_thread(sp_session *session) {
    addTask(process_events, "Notify main thread: process_events");
}

static sp_session_callbacks callbacks {
    &logged_in,
    NULL,
    NULL,
    NULL,
    NULL,
    &notify_main_thread,
    &music_delivery,
    NULL,
    NULL,
    NULL,
};

static void libspotify_loop(sp_session *session) {
    while(true) {
        pthread_mutex_lock(&s_notify_mutex);

        if(s_tasks.size() == 0) {
        }
        list<Task> tasks_copy = s_tasks;
        s_tasks.clear();
        pthread_mutex_unlock(&s_notify_mutex);

        for(list<Task>::iterator it = tasks_copy.begin(); it != tasks_copy.end(); it++) {
            Task task = (*it);
            log("Running task: %s", task.name.c_str());
            task.fptr(task.int_params, task.string_params, session, s_track);
        }
    }
}

void* start_spotify(void *storage_location) {
    string path = (char *)storage_location;

    pthread_mutex_init(&s_notify_mutex, NULL);
    pthread_cond_init(&s_notify_cond, NULL);

    sp_session *session;
    sp_session_config config;

    memset(&config, 0, sizeof(config));

    string cache_location = path + "/cache";
    string settings_location = path + "/settings";

    config.api_version = SPOTIFY_API_VERSION;
    config.cache_location = cache_location.c_str();
    config.settings_location = settings_location.c_str();
    config.application_key = g_appkey;
    config.application_key_size = g_appkey_size;
    config.user_agent = "BSPSpotify";
    config.callbacks = &callbacks;
    config.tracefile = NULL;

    sp_error errorcode = sp_session_create(&config, &session);
    log("BSP Spotify was initiated");

    if(SP_ERROR_OK != errorcode) {
        exitl("failed to create session: %s\n", sp_error_message(errorcode));
    }
    libspotify_loop(session);
}
