#ifndef __JNI_INTERFACE_H__
#define __JNI_INTERFACE_H__

#include <jni.h>

jclass findClass(JNIEnv *inEnv, const char* name);
JNIEnv* getEnv();

extern "C" {
    JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM *jvm, void *reserved);
    JNIEXPORT void JNICALL Java_com_broadspeed_carcontrol_SpotifyWrapper_init(JNIEnv *je, jobject thiz, jstring j_storage_path);
    JNIEXPORT void JNICALL Java_com_broadspeed_carcontrol_SpotifyWrapper_login(JNIEnv *env, jclass jc, jstring j_username, jstring j_password);
    JNIEXPORT void JNICALL Java_com_broadspeed_carcontrol_SpotifyWrapper_searchPlaylist(JNIEnv *env, jclass jc, jint j_playlistIndex);
    JNIEXPORT void JNICALL Java_com_broadspeed_carcontrol_SpotifyWrapper_togglePlay(JNIEnv *env, jclass jc, jint track_id);
}

#endif
