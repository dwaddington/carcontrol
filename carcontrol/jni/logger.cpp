#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <android/log.h>

#include "logger.h"

void log(const char *fmt, ...) {
        va_list args;
        va_start(args, fmt);
        char buf[1000];
        vsprintf(buf, fmt, args);
        __android_log_print(ANDROID_LOG_VERBOSE, "Spotify", "%s, thread %d", buf, pthread_self());
        va_end(args);
}

void logPlayback(const char *fmt, ...) {
        va_list args;
        va_start(args, fmt);
        log(fmt, args);
        va_end(args);
}

void logPlayTrack(const char *fmt, ...) {
        va_list args;
        va_start(args, fmt);
        log(fmt, args);
        va_end(args);
}

// Log message and exit
void exitl(const char *fmt, ...) {
    va_list args;
    va_start(args, fmt);
    log(fmt, args);
    va_end(args);
    exit(1);
}
