package com.broadspeed.carcontrol.camera;

import android.graphics.Bitmap;

public interface Webcam {
    public Bitmap getFrame();
    public void reconnect();
    public void stop();
    public boolean isAttached();
}
