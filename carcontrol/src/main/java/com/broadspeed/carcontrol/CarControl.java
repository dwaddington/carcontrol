package com.broadspeed.carcontrol;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.ContextWrapper;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.broadspeed.carcontrol.camera.Webcam;
import com.broadspeed.carcontrol.model.FragmentRegistry;
import com.broadspeed.carcontrol.model.MusicModel;
import com.broadspeed.carcontrol.model.SptfyPlaylist;
import com.broadspeed.carcontrol.model.SptfyTrack;
import com.broadspeed.carcontrol.util.GForceInterrupt;
import com.broadspeed.carcontrol.view.Cameras.Camera;
import com.broadspeed.carcontrol.view.Home.Home;
import com.broadspeed.carcontrol.view.music.CurrentlyPlaying;
import com.broadspeed.carcontrol.view.music.Device;
import com.broadspeed.carcontrol.view.music.Music;
import com.broadspeed.carcontrol.view.music.Playlists;
import com.broadspeed.carcontrol.view.music.Radio;
import com.broadspeed.carcontrol.view.music.Spotify;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Currently removed the camera stuff so it's not running while i'm working out the G-force POI's
 */

public class CarControl extends FragmentActivity {

    FragmentManager fm = getFragmentManager();
    FragmentRegistry fragReg = new FragmentRegistry(fm);
    Home home_container = new Home(fragReg);
    Music music_container = new Music(fragReg);
    Camera camera_container;
    //Camera camera_container = new Camera(fragReg);
    MusicModel musicModel = new MusicModel();
    SpotifyWrapper sptfy = new SpotifyWrapper(this, musicModel);
    Spotify sptfy_parent_view;
    CurrentlyPlaying currentlyPlaying;
    boolean started = false;

    @Override
    protected void onStart() {
        super.onStart();

        if(!started) {
            File root = new File(Environment.getExternalStorageDirectory(), "carcontrols");
            if(!root.mkdirs()) {
                Log.e("Test", "Failed to create directory");
            }
            File extCache = new File(root.getAbsolutePath(), "cache");
            extCache.mkdirs();
            File extSettings = new File(root.getAbsolutePath(), "settings");
            extSettings.mkdirs();
            File traceFile = new File(extCache.getAbsolutePath(), "trace.txt");
            try {
                traceFile.createNewFile();
            } catch (IOException e) {
                Log.e("Test", "failed to create trace file");
            }


            Log.e("Test", root.getAbsolutePath());
            sptfy.init(root.getAbsolutePath());
            sptfy.login("davidwaddington", "waddyisreallycoo");

            initHome();

            //Call Music Code
            initMusic();

            //Call Camera Code
            //initCamera();

            //fragReg.hideHomeContainer();
            fragReg.hideMusicContainer();
            fragReg.hideCameraContainer();

            GForceInterrupt gf = new GForceInterrupt(this.getBaseContext());
            started = true;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_control);
    }

    private void initHome() {
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.parent_view_container, home_container);
        ft.commit();

        fragReg.setHomeContainer(home_container);
    }

    private void initMusic() {
        //Inflate the fragment and pap it on the screen
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.parent_view_container, music_container);
        ft.commit();
        fm.executePendingTransactions();

        FragmentTabHost fmtTab = music_container.getFragmentTabs();
        fmtTab.setup(this, getSupportFragmentManager(), R.id.realtabcontent);
        fmtTab.addTab(fmtTab.newTabSpec("radio").setIndicator(createTabIndicator("Radio")), Radio.class, null);
        fmtTab.addTab(fmtTab.newTabSpec("spotify").setIndicator(createTabIndicator("Spotify")), Spotify.class, null);

        fragReg.setMusicContainer(music_container);
    }

    private void initCamera() {
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.parent_view_container, camera_container);
        ft.commit();
        fm.executePendingTransactions();
        camera_container.startCamera();

        fragReg.setCameraContainer(camera_container);
    }

    private View createTabIndicator(String text) {
        View view = LayoutInflater.from(this).inflate(R.layout.music_tabs_bg, null);
        TextView t = (TextView) view.findViewById(R.id.tabsText);
        t.setText(text);
        return view;
    }

    //Spotify specific functions
    public void updateSpotifyPlaylists() {
        Log.e("Test", "Callback to draw the spotify playlists to the screen. There are " + musicModel.getPlaylistCount() + " needing done");
        ArrayList<SptfyPlaylist> sptfy_playlists = musicModel.getSpotifyPlaylist();
        for(int i = 0; i < sptfy_playlists.size(); i++) {
            SptfyPlaylist tmp = sptfy_playlists.get(i);
            Log.e("Test", "Index: " + tmp.getBackendIndex() + " Playlist name: " + tmp.getName() + " |\t Playlist Desc: " + tmp.getDesc() + " |\t Playlist Track Count: " + tmp.getTrackCount());
        }
    }

    public ArrayList<SptfyPlaylist> getSpotifyPlaylists(){
        return musicModel.getSpotifyPlaylist();
    }

    public ArrayList<SptfyTrack> getSpotifyTracks() {
        Log.e("Test", "Called getSpotifyTracks()");
        return musicModel.getSpotifyTracks();
    }

    public void loadPlaylistViaJNI(int index, int track_count, Playlists callbackObject) {
        sptfy.searchPlaylistWrapper(index, track_count, callbackObject);
    }

    public void selectTrack(int track_index){
        Log.e("Test", "Select track called with index " + track_index);
        musicModel.setCur_sptfy_index(track_index);
        musicModel.setCur_playing(track_index);
        sptfy.togglePlayWrapper(track_index);
    }

    public void bindBack(String s, Playlists p){
        music_container.rebindBack(s,p);
    }

    public MusicModel getMusicModel() {
        return musicModel;
    }

    public void registerCurrentPlaying(CurrentlyPlaying cp) {
        this.currentlyPlaying = cp;
    }

    public void current_albumart_updated() {
        if(this.currentlyPlaying != null) {
            this.currentlyPlaying.songUpdate();
        }
        if(this.sptfy_parent_view != null) {
            this.sptfy_parent_view.updateTrackInfo(musicModel.getCur_playing());
        }
    }

    public void registerSpotifyParent(Spotify sp) {
        sptfy_parent_view = sp;
    }
}
