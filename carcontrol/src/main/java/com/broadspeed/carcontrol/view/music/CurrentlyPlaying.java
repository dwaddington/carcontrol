package com.broadspeed.carcontrol.view.music;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.broadspeed.carcontrol.CarControl;
import com.broadspeed.carcontrol.R;
import com.broadspeed.carcontrol.model.MusicModel;
import com.broadspeed.carcontrol.model.SptfyTrack;


public class CurrentlyPlaying extends android.support.v4.app.Fragment {
    CurrentlyPlaying thiz = this;
    View rootView;
    CarControl parent;
    MusicModel mModel;
    boolean prevLoaded = false;
    SptfyTrack current_playing = null;
    Spotify sptfyParent;
    boolean playing;
    ImageButton play, rwd, fwd;

    public CurrentlyPlaying(Spotify sptfyParent, CarControl p) {
        this.sptfyParent = sptfyParent;
        if(!prevLoaded) {
            prevLoaded = true;
            parent = p;
            mModel = p.getMusicModel();
            parent.registerCurrentPlaying(thiz);
            current_playing = mModel.getCur_playing();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.e("Test", "CurrentlyPlaying onCreateView()");
        rootView = (ViewGroup) inflater.inflate(R.layout.music_currently_playing, null);
        rwd = (ImageButton) rootView.findViewById(R.id.rwdButton);
        rwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("Test", "Rewind clicked");
                int cur_index = mModel.getCur_sptfy_index();
                if(cur_index == 0) {
                    cur_index = 0;
                } else {
                    cur_index--;
                }
                parent.selectTrack(cur_index);
            }
        });

        fwd = (ImageButton) rootView.findViewById(R.id.fwdButton);
        fwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("Test", "Forward clicked");
                int cur_index = mModel.getCur_sptfy_index();
                if(cur_index == parent.getSpotifyTracks().size()) {
                    cur_index = 0;
                } else {
                    cur_index++;
                }
                parent.selectTrack(cur_index);
            }
        });
        return rootView;
    }

    public void songUpdate() {
        current_playing = mModel.getCur_playing();
        bindPlayButton();
        albumartupdate();
    }

    public void bindPlayButton() {
        play = (ImageButton) rootView.findViewById(R.id.playpauseButton);
        playing = mModel.isPlaying();
        Log.e("Test", "Playing boolean = " + playing);
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(playing) {
                    play.setBackgroundResource(R.drawable.pause);
                } else {
                    play.setBackgroundResource(R.drawable.play);
                }
                play.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(playing) {
                            if(mModel.getCur_sptfy_index() >= 0) {
                                parent.selectTrack(mModel.getCur_sptfy_index());
                                mModel.setPlaying(false);
                                play.setBackgroundResource(R.drawable.play);
                            }
                        } else {
                            if(mModel.getCur_sptfy_index() >= 0) {
                                parent.selectTrack(mModel.getCur_sptfy_index());
                                mModel.setPlaying(true);
                                play.setBackgroundResource(R.drawable.pause);
                            }
                        }

                    }
                });
            }
        });
    }

    public void albumartupdate() {
        current_playing = mModel.getCur_playing();
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Bitmap bmp = current_playing.getAlbumArt();
                ImageView imgV = (ImageView) rootView.findViewById(R.id.sptfy_current_playing_spot__albumart);
                imgV.setImageBitmap(bmp);
            }
        });
    }
}
