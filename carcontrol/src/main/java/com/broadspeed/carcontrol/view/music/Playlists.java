package com.broadspeed.carcontrol.view.music;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;

import com.broadspeed.carcontrol.CarControl;
import com.broadspeed.carcontrol.R;
import com.broadspeed.carcontrol.model.SptfyPlaylist;
import com.broadspeed.carcontrol.model.SptfyTrack;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Playlists extends android.support.v4.app.Fragment {

    Playlists thiz = this;
    boolean fuckBool = false;
    View rootView;
    ArrayAdapter<SptfyPlaylist> adapter;
    ArrayAdapter<SptfyTrack> adapter2;
    CarControl parent;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = (ViewGroup) inflater.inflate(R.layout.music_spotify_playlists, null);
        ListView listView = (ListView) rootView.findViewById(R.id.playlistlists);
        if(!fuckBool) {
            fuckBool = true;
            parent = (CarControl) getActivity();
            ArrayList<SptfyPlaylist> sptfyPlaylists = parent.getSpotifyPlaylists();
            Comparator comp = new Comparator() {
                @Override
                public int compare(Object o, Object o2) {
                    SptfyPlaylist s1 = (SptfyPlaylist) o;
                    SptfyPlaylist s2 = (SptfyPlaylist) o2;
                    return s1.getName().compareTo(s2.getName());
                }
            };
            Collections.sort(sptfyPlaylists, comp);
            Log.e("Test", "" + sptfyPlaylists.size());
            ArrayAdapter<SptfyPlaylist> adapter = new ArrayAdapter<SptfyPlaylist>(this.getActivity(),android.R.layout.simple_list_item_1,sptfyPlaylists);
            listView.setAdapter(adapter);
        }
        return rootView;
    }

    public void loadTracksForPlaylist(){
        parent.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ListView listView = (ListView) rootView.findViewById(R.id.playlistlists);
                ArrayList<SptfyTrack> tracks = parent.getSpotifyTracks();
                adapter2 = new ArrayAdapter<SptfyTrack>(parent,R.layout.music_spotify_playlist_list_item, R.id.playlistText,tracks);
                listView.setAdapter(adapter2);
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        Log.e("Test", "Clicked: " + adapter2.getItem(i).getName());
                        parent.selectTrack(i);
                    }

                });
            }
        });

    }

    public void onResume(){
        super.onResume();
        renderPlaylists();
    }

    public void renderPlaylists(){
        Log.e("test2", "Render Playlist call");
        ListView listView = (ListView) rootView.findViewById(R.id.playlistlists);
        ArrayList<SptfyPlaylist> sptfyPlaylists = parent.getSpotifyPlaylists();
        Comparator comp = new Comparator() {
            @Override
            public int compare(Object o, Object o2) {
                SptfyPlaylist s1 = (SptfyPlaylist) o;
                SptfyPlaylist s2 = (SptfyPlaylist) o2;
                return s1.getName().compareTo(s2.getName());
            }
        };
        Collections.sort(sptfyPlaylists, comp);
        Log.e("Test", "" + sptfyPlaylists.size());
        adapter = new ArrayAdapter<SptfyPlaylist>(this.getActivity(),R.layout.music_spotify_playlist_list_item, R.id.playlistText,sptfyPlaylists);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Log.e("Test", "Clicked: " + adapter.getItem(i).getBackendIndex());
                parent.loadPlaylistViaJNI(adapter.getItem(i).getBackendIndex(), adapter.getItem(i).getTrackCount(), thiz);
                parent.bindBack("Playlists", thiz);
            }
        });
    }
}
