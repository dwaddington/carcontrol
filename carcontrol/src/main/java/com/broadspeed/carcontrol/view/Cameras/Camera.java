package com.broadspeed.carcontrol.view.Cameras;

import android.app.Fragment;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.broadspeed.carcontrol.CarControl;
import com.broadspeed.carcontrol.R;
import com.broadspeed.carcontrol.camera.Clip;
import com.broadspeed.carcontrol.camera.ShellCallback;
import com.broadspeed.carcontrol.camera.UVCCamera;
import com.broadspeed.carcontrol.camera.Webcam;
import com.broadspeed.carcontrol.model.FragmentRegistry;
import com.broadspeed.carcontrol.util.CircularArrayList;
import com.broadspeed.carcontrol.util.CircularStuff;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class Camera extends Fragment {

    FragmentRegistry fragReg;
    Webcam front = null;
    View rootView;
    Handler handler;
    Runnable runnable;
    int interval = 33;
    ImageButton back;
    CircularStuff<byte[]> sieve = new CircularStuff<byte[]>(2000);
    boolean recording = true;

    private File mFileBinDir;
    private String mFfmpegBin;
    private String mCmdCat;
    File temp;

    public Camera(FragmentRegistry f) {
        this.fragReg = f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = (ViewGroup) inflater.inflate(R.layout.camera_container, null);
        gainPermissionOnCameras();
        front = new UVCCamera("/dev/video3");
        back = (ImageButton)rootView.findViewById(R.id.back_camera);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*fragReg.hideCameraContainer();
                fragReg.showHomeContainer();*/

                recording = false;
                Object[] t = sieve.getContentsAndEmpty();
                byte[][] images = new byte[t.length][];
                for(int i = 0; i < t.length; i++) {
                    images[i] = (byte[]) t[i];
                }
                File root = new File(Environment.getExternalStorageDirectory(), "tmp_cc_images");
                if(!root.mkdirs()) {
                    Log.e("Test", "Failed to create directory");
                }
                FileOutputStream fos;
                for(int i = 0; i < t.length; i++) {
                    try {
                        fos = new FileOutputStream(new File(root.getAbsolutePath() + "/image" + i + ".jpg"));
                        fos.write(images[i]);
                        fos.close();
                    } catch(IOException e ){
                        e.printStackTrace();
                    }
                }
                Log.e("Test", "Finished creating jpg files. Total created: " + t.length);

                checkmmfpegBinary(new File(getActivity().getApplicationContext().getApplicationInfo().dataDir));
                List<String> cmd = new ArrayList<String>();
                cmd.add(mFfmpegBin);
                cmd.add("-r");
                cmd.add("15");
                cmd.add("-i");
                cmd.add(root.getAbsolutePath() + "/image%d.jpg");
                cmd.add("-c:v");
                cmd.add("libx264");
                cmd.add(root.getAbsolutePath() + "/test.mp4");
                try {
                    execFFMPEG(cmd);
                } catch(InterruptedException e) {
                    e.printStackTrace();
                } catch(IOException e) {
                    e.printStackTrace();
                }

                for(int i = 0; i < t.length; i++) {
                    File f = new File(root.getAbsolutePath() + "/image" + i + ".jpg");
                    f.delete();
                }
                Log.e("Test", "Finished recording at user button press");
            }
        });
        return rootView;
    }

    private void checkmmfpegBinary(File fileAppRoot) {
        try {
            mFileBinDir = new File(fileAppRoot,"lib");
            if (mFileBinDir.exists())
            {
                File fileBin = new File(mFileBinDir,"libffmpeg.so");
                if (fileBin.exists())
                    mFfmpegBin = fileBin.getCanonicalPath();
                else
                    mFfmpegBin = "ffmpeg";
            }
            else
            {
                mFfmpegBin = "ffmpeg";
            }
            mCmdCat = "sh cat";
        }catch(IOException e) {
            e.printStackTrace();
        }
    }

    private void execFFMPEG (List<String> cmd) throws IOException, InterruptedException {

        String runtimeCmd = new File(mFileBinDir,"ffmpeg").getCanonicalPath();

        Runtime.getRuntime().exec("chmod 777 " +runtimeCmd);

        ShellCallback sc = new ShellCallback() {
            @Override
            public void shellOut(String shellLine) {
                System.out.println("MIX> " + shellLine);
            }

            @Override
            public void processComplete(int exitValue) {
                if (exitValue != 0) {
                    System.err.println("concat non-zero exit: " + exitValue);
                    Log.d("ffmpeg","Compilation error. FFmpeg failed");
                } else {
                    Log.d("ffmpeg","Success");
                }
            }
        };

        execProcess(cmd, sc, mFileBinDir);
    }

    private int execProcess(List<String> cmds, ShellCallback sc, File fileExec) throws IOException, InterruptedException {

        //ensure that the arguments are in the correct Locale format
        for (String cmd :cmds)
        {
            cmd = String.format("%s", cmd);
        }

        ProcessBuilder pb = new ProcessBuilder(cmds);
        pb.directory(fileExec);

        StringBuffer cmdlog = new StringBuffer();

        for (String cmd : cmds)
        {
            cmdlog.append(cmd);
            cmdlog.append(' ');
        }

        sc.shellOut(cmdlog.toString());

        //pb.redirectErrorStream(true);

        Process process = pb.start();


        // any error message?
        StreamGobbler errorGobbler = new StreamGobbler(
                process.getErrorStream(), "ERROR", sc);

        // any output?
        StreamGobbler outputGobbler = new
                StreamGobbler(process.getInputStream(), "OUTPUT", sc);

        errorGobbler.start();
        outputGobbler.start();

        int exitVal = process.waitFor();

        sc.processComplete(exitVal);

        return exitVal;

    }

    private void gainPermissionOnCameras() {
        for(int i = 0; i < 5; i++) {
            String deviceName = "/dev/video" + i;
            File deviceFile = new File(deviceName);
            if(deviceFile.exists()) {
                if(!deviceFile.canRead()) {
                    try {
                        Process p = Runtime.getRuntime().exec(new String[]{"su", "-c", "chmod 0777 " + deviceName});
                    } catch(IOException e) {
                        Log.e("TestApp", "not root");
                    }
                }
            }
        }
    }

    public void startCamera() {
        final long initTime = System.currentTimeMillis();
        final long end = initTime + 1000 * 60;

        if(front.isAttached()) {
            handler = new Handler();
            runnable = new Runnable() {
                @Override
                public void run() {
                    if(recording) {

                        handler.postDelayed(runnable, interval);
                        Bitmap bmp = front.getFrame();
                        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                        String dateTime = sdf.format(Calendar.getInstance().getTime()); // reading local time in the system
                        Canvas cs = new Canvas(bmp);
                        Paint tPaint = new Paint();
                        tPaint.setAntiAlias(true);
                        tPaint.setTextSize(25);
                        tPaint.setColor(Color.WHITE);
                        tPaint.setStyle(Paint.Style.FILL);
                        tPaint.setShadowLayer(1.0f, 1.0f, 1.0f, Color.BLACK);
                        float height = tPaint.measureText("yY");
                        cs.drawText(dateTime, 20f, height+15f, tPaint);

                        bmp.prepareToDraw();
                        ByteArrayOutputStream out = new ByteArrayOutputStream();
                        bmp.compress(Bitmap.CompressFormat.JPEG, 75, out);
                        sieve.add(out.toByteArray());

                        ImageView iView = (ImageView) rootView.findViewById(R.id.camera_canvas);
                        iView.setImageBitmap(bmp);
                    }
                }
            };
            handler.postAtTime(runnable, System.currentTimeMillis()+interval);
            handler.postDelayed(runnable, interval);
        } else {
            front = new UVCCamera("/dev/video3");
            startCamera();
        }
    }

    private class StreamGobbler extends Thread
    {
        InputStream is;
        String type;
        ShellCallback sc;

        StreamGobbler(InputStream is, String type, ShellCallback sc)
        {
            this.is = is;
            this.type = type;
            this.sc = sc;
        }

        public void run()
        {
            try
            {
                InputStreamReader isr = new InputStreamReader(is);
                BufferedReader br = new BufferedReader(isr);
                String line=null;
                while ( (line = br.readLine()) != null)
                    if (sc != null)
                        sc.shellOut(line);

            } catch (IOException ioe)
            {
                ioe.printStackTrace();
            }
        }
    }
}
