package com.broadspeed.carcontrol.view.Home;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.broadspeed.carcontrol.R;
import com.broadspeed.carcontrol.model.FragmentRegistry;


public class HomeMusic extends Fragment {
    FragmentRegistry fragReg;
    public HomeMusic(FragmentRegistry fragReg) {
        this.fragReg = fragReg;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.home_music, container, false);
        Button btn = (Button) rootView.findViewById(R.id.load_music);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragReg.hideHomeContainer();
                fragReg.showMusicContainer();
            }
        });

        Button btn2 = (Button) rootView.findViewById(R.id.load_camera);
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragReg.hideHomeContainer();
                fragReg.showCameraContainer();
            }
        });
        return rootView;
    }
}
