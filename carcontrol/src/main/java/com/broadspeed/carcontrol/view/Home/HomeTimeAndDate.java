package com.broadspeed.carcontrol.view.Home;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.broadspeed.carcontrol.R;
import com.broadspeed.carcontrol.model.FragmentRegistry;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class HomeTimeAndDate extends Fragment {
    private Timer timer;
    TextView time_text;
    TextView date_display;
    FragmentRegistry fragReg;

    public HomeTimeAndDate(FragmentRegistry fragReg) {
        this.fragReg = fragReg;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.home_time_and_date, container, false);
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        time_text = (TextView) getActivity().findViewById(R.id.time_display);
        date_display = (TextView) getActivity().findViewById(R.id.date_display);
        timer = new Timer("DigitalClock");
        Calendar c = Calendar.getInstance();

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                date_display.setText(getCurrentDate());
            }
        });

        final Runnable updater = new Runnable() {
            @Override
            public void run() {
                if(getCurrentTimeString().equals("00:00")) {
                    date_display.setText(getCurrentDate());
                }
                time_text.setText(getCurrentTimeString());
            }
        };

        int msec = 999 - c.get(Calendar.MILLISECOND);
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                getActivity().runOnUiThread(updater);
            }
        }, msec, 1000);
    }

    private String getCurrentTimeString() {
        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        return String.format("%02d:%02d", hour, minute);
    }

    private String getCurrentDate() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dayOfWeek = new SimpleDateFormat("EEEE, d MMMM yyyy", Locale.getDefault());
        return dayOfWeek.format(calendar.getTime());
    }
}
