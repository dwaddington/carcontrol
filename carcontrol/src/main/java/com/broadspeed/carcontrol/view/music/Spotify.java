package com.broadspeed.carcontrol.view.music;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.FragmentTabHost;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.broadspeed.carcontrol.CarControl;
import com.broadspeed.carcontrol.R;
import com.broadspeed.carcontrol.model.SptfyTrack;

public class Spotify extends android.support.v4.app.Fragment {

    FragmentTabHost fmtTab;
    View internal_view;
    View rootView;
    android.support.v4.app.Fragment cp;
    CarControl parent;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        this.parent = (CarControl) getActivity();
        cp = new CurrentlyPlaying(this, this.parent);
        parent.registerSpotifyParent(this);

        rootView = (ViewGroup) inflater.inflate(R.layout.music_spotify, null);
        fmtTab = (FragmentTabHost) rootView.findViewById(android.R.id.tabhost);
        fmtTab.setup(this.getActivity(), this.getChildFragmentManager(), R.id.spotify_realtabcontent);
        fmtTab.addTab(fmtTab.newTabSpec("playlists").setIndicator(createTabIndicator("Playlists", inflater)), Playlists.class, null);
        //fmtTab.addTab(fmtTab.newTabSpec("search").setIndicator(createTabIndicator("Search", inflater)), Search.class, null);

        android.support.v4.app.FragmentManager fm = this.getFragmentManager();
        android.support.v4.app.FragmentTransaction ft = fm.beginTransaction();
        fm.beginTransaction();
        ft.add(R.id.sptfy_current_playing_spot, cp);
        ft.commit();
        return rootView;
    }

    private View createTabIndicator(String text, LayoutInflater inflater) {
        View view = inflater.inflate(R.layout.music_tabs_bg, null);
        TextView t = (TextView) view.findViewById(R.id.tabsText);
        t.setText(text);
        return view;
    }

    public void updateTrackInfo(SptfyTrack track) {
        TextView track_title = (TextView) rootView.findViewById(R.id.sptfy_track_title);
        String track_title_str = track.getName();
        if(track_title_str.length() >= 30) {
            track_title_str = track_title_str.substring(0, 27) + "...";
        }
        if(!track_title.getText().equals(track_title_str)) {
            track_title.setText(track_title_str);
        }
        TextView track_album = (TextView) rootView.findViewById(R.id.sptfy_album_name);
        String track_album_str = track.getAlbum();
        if(track_album_str.length() >= 40) {
            track_album_str = track_album_str.substring(0, 38) + "...";
        }
        if(!track_album.getText().equals(track_album_str)) {
            track_album.setText(track_album_str);
        }

        TextView track_artist = (TextView) rootView.findViewById(R.id.sptfy_artist_name);
        String track_artist_str = track.getArtist();
        if(track_artist_str.length() >= 40) {
            track_artist_str = track_artist_str.substring(0, 38) + "...";
        }
        if(!track_artist.getText().equals(track_artist_str)) {
            track_artist.setText(track_artist_str);
        }
    }
}
