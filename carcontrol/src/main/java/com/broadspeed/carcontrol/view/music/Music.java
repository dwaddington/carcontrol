package com.broadspeed.carcontrol.view.music;


import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentTabHost;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;


import com.broadspeed.carcontrol.R;
import com.broadspeed.carcontrol.model.FragmentRegistry;

public class Music extends Fragment {
    FragmentTabHost ftabs;
    FragmentRegistry fragReg;
    ImageButton back;

    public Music(FragmentRegistry fragReg) {
        this.fragReg = fragReg;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = (ViewGroup) inflater.inflate(R.layout.music_container, null);
        ftabs = (FragmentTabHost) rootView.findViewById(android.R.id.tabhost);
        back = (ImageButton)rootView.findViewById(R.id.back_music);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragReg.hideMusicContainer();
                fragReg.showHomeContainer();
            }
        });
        return rootView;
    }

    public void rebindBack(String s, final Playlists p){
        if(s.equals("Playlists")){
            back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    p.renderPlaylists();
                    back.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            fragReg.hideMusicContainer();
                            fragReg.showHomeContainer();
                        }
                    });
                }
            });
        }
    }

    public FragmentTabHost getFragmentTabs() {
        return ftabs;
    }

}
