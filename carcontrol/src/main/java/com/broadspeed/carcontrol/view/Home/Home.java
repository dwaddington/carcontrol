package com.broadspeed.carcontrol.view.Home;


import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.broadspeed.carcontrol.R;
import com.broadspeed.carcontrol.model.FragmentRegistry;

public class Home extends Fragment {
    FragmentRegistry fragReg;
    public Home(FragmentRegistry fragReg) {
        this.fragReg = fragReg;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.home_container, container, false);

        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        fm.beginTransaction();
        Fragment time_date = new HomeTimeAndDate(fragReg);
        Fragment music_display = new HomeMusic(fragReg);
        ft.add(R.id.home_time_slot, time_date);
        ft.add(R.id.music_display_slot, music_display);
        ft.commit();

        return rootView;
    }
}
