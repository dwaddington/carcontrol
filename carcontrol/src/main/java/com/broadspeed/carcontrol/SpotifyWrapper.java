package com.broadspeed.carcontrol;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.broadspeed.carcontrol.model.MusicModel;
import com.broadspeed.carcontrol.model.SptfyPlaylist;
import com.broadspeed.carcontrol.model.SptfyTrack;
import com.broadspeed.carcontrol.view.music.Playlists;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;

public class SpotifyWrapper {
    static {
        System.loadLibrary("spotify");
        System.loadLibrary("spotifywrapper");
    }

    static MusicModel musicModel;
    static CarControl parent;
    static int sptfy_playlist_count = 0;
    static int sptfy_playlist_track_count = 0;
    static int last_played_index = -1;

    static Playlists playlist_callback;

    public SpotifyWrapper(CarControl parent, MusicModel m) {
        this.parent = parent;
        musicModel = m;
    }

    native public static void init(String location);
    native public static void login(String username, String password);
    native public static void searchPlaylist(int index);
    native public static void togglePlay(int index);

    public static void togglePlayWrapper(int index) {
        Log.e("Test", "last_played_index == index -> " + (last_played_index == index));
        if(last_played_index == index) {
            musicModel.setPlaying(!musicModel.isPlaying());
            Log.e("Test", "Music model set to " + musicModel.isPlaying());
            parent.current_albumart_updated();
        } else {
            last_played_index = index;
            musicModel.setPlaying(true);
            parent.current_albumart_updated();
        }
        togglePlay(index);
    }

    public static void loggedIn_cback(String message) {
        Log.e("Test", "Spotify error message: " + message);
    }

    public static void loadTrackImage(byte[] data) {
        ByteArrayInputStream imageStream = new ByteArrayInputStream(data);
        Bitmap bitmap = BitmapFactory.decodeStream(imageStream);
        musicModel.setCur_playing_albumart(bitmap);
        Log.e("Test", "Calling CarControl albumart_updated()");
        parent.current_albumart_updated();
    }

    public static void searchPlaylistWrapper(int index, int track_count, Playlists callbackObject) {
        sptfy_playlist_track_count = track_count;
        playlist_callback = callbackObject;
        musicModel.clearSpotifyTracks();
        Log.e("Test", "Track count set to: " + sptfy_playlist_track_count);
        searchPlaylist(index);
    }

    public static void addPlaylist(int index, String playlist_name, String description, int no_of_tracks) {
        if(musicModel.getPlaylistCount() == sptfy_playlist_count) {
            finishedPlaylistLoad();
        } else {
            Log.e("Test", "Called addPlaylist() Method");
            SptfyPlaylist pl = new SptfyPlaylist();
            pl.setBackendIndex(index);
            pl.setName(playlist_name);
            pl.setDescription(description);
            pl.setTrackCount(no_of_tracks);
            musicModel.addPlaylist(pl);
        }
    }

    public static void setPlaylistCount(int count) {
        sptfy_playlist_count = count;
        Log.e("Test", "Playlist count set to: " + sptfy_playlist_count);
    }

    public static void finishedPlaylistLoad() {
        parent.updateSpotifyPlaylists();
    }

    public static void finishedTrackLoading() {
        Log.e("Test", "Called loadTracksForPlaylist()");
        playlist_callback.loadTracksForPlaylist();
    }

    public static void addTrackToCurPlaylist(String track_name, String artist_name, String album_name, int duration) {
        if((musicModel.getTrackCount()+1) == sptfy_playlist_track_count) {
            SptfyTrack track = new SptfyTrack();
            track.setName(track_name);
            track.setArtist(artist_name);
            track.setAlbum(album_name);
            track.setDuration(duration);
            musicModel.addSpotifyTrack(track);
            finishedTrackLoading();
        } else {
            SptfyTrack track = new SptfyTrack();
            track.setName(track_name);
            track.setArtist(artist_name);
            track.setAlbum(album_name);
            track.setDuration(duration);
            musicModel.addSpotifyTrack(track);
        }
    }
}
