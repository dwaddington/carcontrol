package com.broadspeed.carcontrol.model;

public class SptfyPlaylist {
    int backendIndex;
    String name;
    String description;
    int number_of_tracks;

    public void setBackendIndex(int in) { this.backendIndex = in; }
    public int getBackendIndex() { return this.backendIndex; }
    public void setName(String in) {
        name = in;
    }
    public String getName() {
        return name;
    }
    public void setDescription(String in) {
        description = in;
    }
    public String getDesc() {
        return description;
    }
    public void setTrackCount(int in) {
        number_of_tracks = in;
    }
    public int getTrackCount() {
        return number_of_tracks;
    }
    @Override
    public String toString() { return this.name; }
}
