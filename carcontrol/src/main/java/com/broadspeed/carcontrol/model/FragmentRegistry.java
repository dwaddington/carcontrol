package com.broadspeed.carcontrol.model;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;

import com.broadspeed.carcontrol.view.Cameras.Camera;
import com.broadspeed.carcontrol.view.Home.Home;
import com.broadspeed.carcontrol.view.music.Music;

public class FragmentRegistry {
    Home home_container;
    Music music_container;
    Camera camera_container;
    FragmentManager fm;

    public FragmentRegistry(FragmentManager f) {
        fm = f;
    }

    public void setHomeContainer(Home f) {
        home_container = f;
    }

    public void hideHomeContainer() {
        FragmentTransaction ft = fm.beginTransaction();
        ft.hide(home_container);
        ft.commit();
    }

    public void showHomeContainer() {
        FragmentTransaction ft = fm.beginTransaction();
        ft.show(home_container);
        ft.commit();
    }

    public void setMusicContainer(Music f) {
        music_container = f;
    }

    public void hideMusicContainer() {
        FragmentTransaction ft = fm.beginTransaction();
        ft.hide(music_container);
        ft.commit();
    }

    public void showMusicContainer() {
        FragmentTransaction ft = fm.beginTransaction();
        ft.show(music_container);
        ft.commit();
    }

    public void setCameraContainer(Camera c) { camera_container = c; }

    public void hideCameraContainer() {
        FragmentTransaction ft = fm.beginTransaction();
        ft.hide(camera_container);
        ft.commit();
    }

    public void showCameraContainer() {
        FragmentTransaction ft = fm.beginTransaction();
        ft.show(camera_container);
        ft.commit();
    }

}
