package com.broadspeed.carcontrol.model;

import android.graphics.Bitmap;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

public class MusicModel {
    String type;
    ArrayList<SptfyPlaylist> sptfy_playlists = new ArrayList<SptfyPlaylist>();
    ArrayList<SptfyTrack> sptfy_tracks = new ArrayList<SptfyTrack>();
    SptfyTrack cur_playing = null;
    boolean playing = false;
    int cur_sptfy_index = 0;

    public MusicModel() {
        type = "radio";
    }

    public void activateRadio() {
        type = "radio";
    }

    public void activateSpotify() {
        type = "spotify";
    }

    public void activateDevice() {
        type = "device";
    }

    public void addPlaylist(SptfyPlaylist in) {
        this.sptfy_playlists.add(in);
    }

    public int getPlaylistCount() {
        return sptfy_playlists.size();
    }

    public void clearSpotifyTracks() {
        this.sptfy_tracks.clear();
    }

    public void addSpotifyTrack(SptfyTrack in) {
        this.sptfy_tracks.add(in);
    }

    public int getTrackCount() {
        return sptfy_tracks.size();
    }

    public ArrayList<SptfyPlaylist> getSpotifyPlaylist() {
        return sptfy_playlists;
    }

    public ArrayList<SptfyTrack> getSpotifyTracks() { return sptfy_tracks; }

    public int getCur_sptfy_index() {
        return cur_sptfy_index;
    }

    public void setCur_sptfy_index(int cur_sptfy_index) {
        this.cur_sptfy_index = cur_sptfy_index;
    }

    public SptfyTrack getCur_playing() {
        return cur_playing;
    }

    public void setCur_playing(int index) {
        this.cur_playing = sptfy_tracks.get(index);
    }

    public void setCur_playing_albumart(Bitmap b) {
        if(this.cur_playing != null) {
            this.cur_playing.setAlbumArt(b);
        } else {
            Log.e("Test", "Currently Playing is null");
        }
    }

    public void setPlaying(boolean p) {
        this.playing = p;
    }

    public boolean isPlaying() {
        return this.playing;
    }
}
