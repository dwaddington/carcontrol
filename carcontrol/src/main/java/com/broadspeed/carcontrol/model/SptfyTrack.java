package com.broadspeed.carcontrol.model;

import android.graphics.Bitmap;

public class SptfyTrack {
    String name;
    String artist;
    String album;
    Bitmap albumart;
    int duration;

    public void setName(String in) { name = in; }
    public String getName() { return name; }

    public void setArtist(String in) { artist = in; }
    public String getArtist() { return artist; }

    public void setAlbum(String in) { album = in; }
    public String getAlbum() { return album; }

    public void setAlbumArt(Bitmap m) { albumart = m; }
    public Bitmap getAlbumArt() { return albumart; }

    public void setDuration(int in) { duration = in; }
    public int getDuration() { return duration; }

    @Override
    public String toString() { return this.name + " | " + this.artist + " | " + this.album; }
}
