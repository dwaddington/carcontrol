package com.broadspeed.carcontrol.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MySQLiteHelper extends SQLiteOpenHelper {

    public static final String TABLE_BSP = "bsp";
    public static final String COLUMN_ID = "_id";
    public static final String KEY_COLUMN = "key";
    public static final String VALUE_COLUMN = "value";

    private static final String DATABASE_NAME = "bsp.db";
    private static final int DATABASE_VERSION = 1;

    private static final String DATABASE_CREATE = "create table " + TABLE_BSP + "(" + COLUMN_ID + " integer primary key autoincrement, " + KEY_COLUMN + " text not null, " + VALUE_COLUMN + " text not null);";

    public MySQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(MySQLiteHelper.class.getName(), "upgrading database");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BSP);
        onCreate(db);
    }
}
