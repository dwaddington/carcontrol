package com.broadspeed.carcontrol.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DBDataSource {
    private SQLiteDatabase database;
    private MySQLiteHelper dbHelper;
    private String[] allColumns = { MySQLiteHelper.COLUMN_ID, MySQLiteHelper.KEY_COLUMN, MySQLiteHelper.VALUE_COLUMN };

    public DBDataSource(Context context) {
        dbHelper = new MySQLiteHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public DBEntry createEntry(String key, String value) {
        ContentValues values = new ContentValues();
        values.put(MySQLiteHelper.KEY_COLUMN, key);
        values.put(MySQLiteHelper.VALUE_COLUMN, value);

        long insertId = database.insert(MySQLiteHelper.TABLE_BSP, null, values);
        Cursor cursor = database.query(MySQLiteHelper.TABLE_BSP, allColumns, MySQLiteHelper.COLUMN_ID + " = " + insertId, null, null, null, null);
        cursor.moveToFirst();
        DBEntry dbEntry = cursorToEntry(cursor);
        cursor.close();
        return dbEntry;
    }

    public void deleteEntry(DBEntry dbEntry) {
        long id = dbEntry.getId();
        database.delete(MySQLiteHelper.TABLE_BSP, MySQLiteHelper.COLUMN_ID + " = " + id, null);
    }

    public List<DBEntry> getAllDBEntries() {
        List<DBEntry> dbEntries = new ArrayList<DBEntry>();
        Cursor cursor = database.query(MySQLiteHelper.TABLE_BSP, allColumns, null, null, null, null, null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            DBEntry dbEntry = cursorToEntry(cursor);
            dbEntries.add(dbEntry);
            cursor.moveToNext();
        }
        cursor.close();
        return dbEntries;
    }

    public String getValueForKey(String key) {
        Cursor cursor = database.query(MySQLiteHelper.TABLE_BSP, allColumns, MySQLiteHelper.KEY_COLUMN + " = \"" + key + "\"", null, null, null, null);
        if(cursor.getCount() == 0) {
            return null;
        }
        cursor.moveToFirst();
        return cursor.getString(2);
    }

    private DBEntry cursorToEntry(Cursor cursor) {
        DBEntry dbEntry = new DBEntry();
        dbEntry.setId(cursor.getLong(0));
        dbEntry.setKey(cursor.getString(1));
        dbEntry.setValue(cursor.getString(2));
        return dbEntry;
    }
}
