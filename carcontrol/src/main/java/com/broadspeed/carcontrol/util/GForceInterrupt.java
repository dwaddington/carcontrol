package com.broadspeed.carcontrol.util;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;
import android.widget.Toast;

public class GForceInterrupt implements SensorEventListener {
    private SensorManager sensorManager;
    private float[] gravity = new float[3];
    double ax, ay, az;
    private Context context;

    public GForceInterrupt(Context c) {
        context = c;
        sensorManager = (SensorManager) c.getSystemService(Context.SENSOR_SERVICE);
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_FASTEST);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        final float alpha = (float) 0.8;

        if(sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {

            gravity[0] = alpha * gravity[0] + (1 - alpha) * sensorEvent.values[0];
            gravity[1] = alpha * gravity[1] + (1 - alpha) * sensorEvent.values[1];
            gravity[2] = alpha * gravity[1] + (1 - alpha) * sensorEvent.values[2];

            ax = sensorEvent.values[0] - gravity[0];
            ay = sensorEvent.values[1] - gravity[1];
            az = sensorEvent.values[2] - gravity[2];
        }
        double g = Math.sqrt(ax * ax + ay * ay + az * az);
        if(g > 20) {
            Log.e("GForce", "Gforce measured: " + g);
            Toast.makeText(context, "GForce exerted: " + g, Toast.LENGTH_LONG);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}
