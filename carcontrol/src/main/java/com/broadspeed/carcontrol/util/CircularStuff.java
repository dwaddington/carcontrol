package com.broadspeed.carcontrol.util;


import java.lang.reflect.Array;

public class CircularStuff<T> {
    private T[] buffer;
    private int final_capacity = 0;
    private int cur_index = 0;

    public CircularStuff(int capacity) {
        final_capacity = capacity;
        buffer = (T[]) new Object[capacity];
    }

    public void add(T in) {
        buffer[cur_index%final_capacity] = in;
        cur_index++;
    }

    public int size() {
        return cur_index%final_capacity;
    }

    public T[] getContentsAndEmpty() {
        int local_count = 0;
        T[] retBuffer;
        int final_size = 0;
        if(cur_index < final_capacity) {
            final_size = cur_index;
        } else {
            final_size = final_capacity;
        }
        retBuffer = (T[]) new Object[final_size];
        while(local_count < final_size) {
            retBuffer[local_count] = buffer[cur_index%final_size];
            buffer[cur_index%final_size] = null;
            cur_index++;
            local_count++;
        }
        return retBuffer;
    }
}
